// Link.react.test.js
import React from 'react'
import { Login } from '../'
import { shallow } from 'enzyme'

describe('Login Tests', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  const setState = jest.fn()
  const useStateSpy = jest.spyOn(React, 'useState')
  useStateSpy.mockImplementation(init => [init, setState])

  it('should match snapshot', () => {
    const component = shallow(<Login />)
    expect(component).toMatchSnapshot()
  })

  it('should login user', () => {
    const loginUser = jest.fn()
    const component = shallow(<Login loginUser={loginUser} />)

    component
      .find('Input')
      .at(0)
      .simulate('change', { target: { value: 'SOME_EMAIL' } })
    expect(
      component
        .find('Input')
        .at(0)
        .prop('value')
    ).toBe('SOME_EMAIL')
    component
      .find('Input')
      .at(1)
      .simulate('change', { target: { value: 'SOME_PASSWORD' } })
    expect(
      component
        .find('Input')
        .at(1)
        .prop('value')
    ).toBe('SOME_PASSWORD')

    component.find('Button').simulate('click')
    expect(loginUser).toBeCalled()
  })

  it('should show error if login fails', async () => {
    const loginUser = jest
      .fn()
      .mockImplementation(() => Promise.reject('ERROR'))
    const component = shallow(<Login loginUser={loginUser} />)

    component
      .find('Input')
      .at(0)
      .simulate('change', { target: { value: 'SOME_EMAIL' } })

    await component.find('Button').simulate('click')

    expect(component).toMatchSnapshot()

    component
      .find('Input')
      .at(1)
      .simulate('change', { target: { value: 'SOME_PASSWORD' } })

    await component.find('Button').simulate('click')

    expect(component).toMatchSnapshot()
  })

  it('should show register form', async () => {
    const component = shallow(<Login />)

    component.find('Text').simulate('click')

    expect(component).toMatchSnapshot()
  })

  it('should hide register form', async () => {
    const component = shallow(<Login />)

    component.find('Text').simulate('click')

    component.find('Text').simulate('click')

    expect(component).toMatchSnapshot()
  })

  it('should register user', async () => {
    const registerUser = jest.fn()
    const component = shallow(<Login registerUser={registerUser} />)

    component.find('Text').simulate('click')

    component
      .find('Input')
      .at(0)
      .simulate('change', { target: { value: 'SOME_NAME' } })
    expect(
      component
        .find('Input')
        .at(0)
        .prop('value')
    ).toBe('SOME_NAME')

    component
      .find('Input')
      .at(1)
      .simulate('change', { target: { value: 'SOME_EMAIL' } })
    expect(
      component
        .find('Input')
        .at(1)
        .prop('value')
    ).toBe('SOME_EMAIL')

    component
      .find('Input')
      .at(2)
      .simulate('change', { target: { value: 'SOME_PASSWORD' } })
    expect(
      component
        .find('Input')
        .at(2)
        .prop('value')
    ).toBe('SOME_PASSWORD')

    component
      .find('Input')
      .at(3)
      .simulate('change', { target: { value: 'SOME_PASSWORD' } })
    expect(
      component
        .find('Input')
        .at(3)
        .prop('value')
    ).toBe('SOME_PASSWORD')

    await component.find('Button').simulate('click')

    expect(registerUser).toBeCalled()
  })

  it('should show error if register fails', async () => {
    const registerUser = jest
      .fn()
      .mockImplementation(() => Promise.reject('ERROR'))
    const component = shallow(<Login registerUser={registerUser} />)

    component.find('Text').simulate('click')

    await component.find('Button').simulate('click')

    expect(component).toMatchSnapshot()

    component
      .find('Input')
      .at(0)
      .simulate('change', { target: { value: 'SOME_NAME' } })

    component
      .find('Input')
      .at(1)
      .simulate('change', { target: { value: 'SOME_EMAIL' } })

    component
      .find('Input')
      .at(2)
      .simulate('change', { target: { value: 'SOME_PASSWORD' } })

    component
      .find('Input')
      .at(3)
      .simulate('change', { target: { value: 'SOME_WRONG_PASSWORD' } })

    await component.find('Button').simulate('click')

    expect(component).toMatchSnapshot()

    component
      .find('Input')
      .at(3)
      .simulate('change', { target: { value: 'SOME_PASSWORD' } })

    await component.find('Button').simulate('click')

    expect(component).toMatchSnapshot()
  })
})
