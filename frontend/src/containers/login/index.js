import { Button, Icon, Input, Typography } from 'antd'
import { Alert } from 'antd'
import React, { useState } from 'react'
import { connect } from 'react-redux'
import { createUser, login } from '../../state/actions/auth'
import './index.css'
const { Text } = Typography

export function Login({ loginUser, registerUser }) {
  const [error, setError] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')
  const [name, setName] = useState('')
  const [register, setRegister] = useState(false)

  const doRegister = async () => {
    try {
      if (email && password && repeatPassword && name)
        if (password !== repeatPassword)
          setError({ message: 'Passwords do not match' })
        else await registerUser(email, password, name)
      else setError({ message: 'Please fill all fields' })
    } catch (err) {
      setError(err)
    }
  }

  const doLogin = async () => {
    try {
      if (email && password) await loginUser(email, password)
      else setError({ message: 'Please insert email and password' })
    } catch (err) {
      setError(err)
    }
  }

  return (
    <div className="login-container">
      <img
        className="login-logo"
        src="https://gyant.com/wp-content/uploads/2018/10/Gyant.Logotype.HorizontalLeft@2x-1.png"
        alt="gyant"
      ></img>
      <div className="login-content">
        {register && (
          <Input
            name="name"
            type="text"
            placeholder="name"
            addonAfter={<Icon type="user" />}
            value={name}
            onChange={e => setName(e.target.value)}
          />
        )}
        <Input
          name="username"
          type="text"
          placeholder="email"
          addonAfter={<Icon type="user" />}
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
        <Input
          name="password"
          type="password"
          placeholder="password"
          addonAfter={<Icon type="key" />}
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        {register ? (
          <React.Fragment>
            <Input
              name="password"
              type="password"
              placeholder="repeat password"
              addonAfter={<Icon type="key" />}
              value={repeatPassword}
              onChange={e => setRepeatPassword(e.target.value)}
            />
            <Text
              className="pointer"
              type="warning"
              onClick={() => {
                setEmail('')
                setName('')
                setPassword('')
                setRepeatPassword('')
                setError('')
                setRegister(false)
              }}
            >
              Already have an account
            </Text>
            <Button type="primary" onClick={() => doRegister()}>
              Register
            </Button>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Text
              className="pointer"
              type="warning"
              onClick={() => {
                setEmail('')
                setPassword('')
                setError('')
                setRegister(true)
              }}
            >
              Register
            </Text>
            <Button type="primary" onClick={() => doLogin()}>
              Login
            </Button>
          </React.Fragment>
        )}
        {error && (
          <Alert
            message="Error Authenticating User"
            description={error.message}
            type="error"
            showIcon
          />
        )}
      </div>
    </div>
  )
}

const mapDispatchToProps = () => ({
  loginUser: login,
  registerUser: createUser
})

export default connect(
  null,
  mapDispatchToProps()
)(Login)
