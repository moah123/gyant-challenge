import {
  Button,
  Icon,
  Input,
  notification,
  PageHeader,
  Select,
  Typography
} from 'antd'
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { fetchDiagnostics } from '../../state/actions/diagnosis'
import { fetchEpisodes, nextEpisode } from '../../state/actions/episode'
import { logout } from '../../state/actions/auth'
import './index.css'
import 'antd/dist/antd.css'

const { Option } = Select
const { TextArea } = Input
const { Title } = Typography

export function Home({
  diagnostics,
  episode,
  getDiagnostics,
  getEpisodes,
  loading,
  logoutUser,
  nextEpisode,
  user
}) {
  const [diagnostic, setDiagnostic] = useState()
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    getDiagnostics()
    getEpisodes()
  }, [getDiagnostics, getEpisodes])

  useEffect(() => {
    if (episode === null) openNotification()
    else {
      const timer = setInterval(() => {
        setCounter(prevCount => prevCount + 1)
      }, 1000)
      return () => {
        setCounter(0)
        setDiagnostic(null)
        clearInterval(timer)
      }
    }
  }, [episode])

  const openNotification = () => {
    notification.open({
      message: 'You are done',
      description: 'Thank you for labeling our cases.',
      icon: <Icon type="smile" style={{ color: '#108ee9' }} />
    })
  }

  const labelEpisode = () => {
    if (diagnostic) {
      nextEpisode(diagnostic, episode, counter)
    }
  }

  return (
    <div className="home-container">
      <PageHeader
        ghost={false}
        title="Gyant"
        subTitle="Challenge"
        extra={[
          <React.Fragment key={0}>
            <span level="4">Logged in as {user.name}</span> |
            <span className="pointer" onClick={logoutUser}>
              <u>Logout</u>
            </span>
          </React.Fragment>
        ]}
      ></PageHeader>
      <div className="home-content">
        <div className="review">
          <Title level={2}>Please Review This Case</Title>
          <TextArea rows={20} value={episode ? episode.text : ''} />
        </div>
        <div className="condition">
          <Title level={2}>Select Condition</Title>
          <div>
            <Select
              style={{ width: 350 }}
              loading={loading}
              placeholder="Select a Condition"
              onChange={value => {
                setDiagnostic(diagnostics.find(diag => diag.code === value))
              }}
              value={diagnostic ? diagnostic.code : undefined}
              filterOption={(input, option) =>
                option.props.children
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
              showSearch
            >
              {diagnostics &&
                diagnostics.length > 0 &&
                diagnostics.map((diagnosis, idx) => (
                  <Option key={idx} value={diagnosis.code}>
                    {diagnosis.description}
                  </Option>
                ))}
            </Select>
          </div>
          <Button
            onClick={labelEpisode}
            value="Next Case"
            disabled={episode === null}
          >
            Next Case
          </Button>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  diagnostics: state.diagnosis,
  episode: state.selectedEpisode,
  loading: state.loading,
  user: state.auth ? state.auth.user : null
})

const mapDispatchToProps = () => ({
  logoutUser: logout,
  getDiagnostics: fetchDiagnostics,
  getEpisodes: fetchEpisodes,
  nextEpisode: nextEpisode
})

export default connect(
  mapStateToProps,
  mapDispatchToProps()
)(Home)
