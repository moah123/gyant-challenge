// Link.react.test.js
import React from 'react'
import HomeConnected, { Home } from '../'
import configureStore from 'redux-mock-store'
import { shallow, mount } from 'enzyme'

jest.useFakeTimers()

const mockStore = configureStore([])

const diagnostics = [
  { code: 'SOME_CODE1', description: 'SOME_DESCRIPTION1' },
  { code: 'SOME_CODE2', description: 'SOME_DESCRIPTION2' }
]
describe('Home Tests', () => {
  it('should match snapshot', () => {
    const store = mockStore({ auth: { user: 'SOME_USER' } })

    const componentConnected = shallow(<HomeConnected store={store} />)

    expect(componentConnected).toMatchSnapshot()

    const component = shallow(
      <Home
        user={{ name: 'SOME_NAME' }}
        getDiagnostics={jest.fn()}
        getEpisodes={jest.fn()}
        diagnostics={diagnostics}
      />
    )
    expect(component).toMatchSnapshot()

    const emptyStore = mockStore({})

    const componentConnectedWithEmptyStore = shallow(
      <HomeConnected store={emptyStore} />
    )

    expect(componentConnectedWithEmptyStore).toMatchSnapshot()
  })

  it('should label episode after pressing Next Case button', () => {
    const diagnostic = diagnostics[0]
    const episode = { text: 'SOME_TEXT' }
    const nextEpisode = jest.fn()
    const component = mount(
      <Home
        user={{ name: 'SOME_NAME' }}
        getDiagnostics={jest.fn()}
        getEpisodes={jest.fn()}
        nextEpisode={nextEpisode}
        diagnostics={diagnostics}
        episode={episode}
      />
    )

    component.find('Button').simulate('click')
    expect(nextEpisode).not.toBeCalled()
    component
      .find('Select')
      .at(0)
      .props()
      .filterOption('SOME_', { props: { children: '' } })
    component
      .find('Select')
      .at(0)
      .props()
      .onChange(diagnostic.code)

    jest.advanceTimersByTime(2000)

    component.find('Button').simulate('click')
    expect(nextEpisode).toBeCalledWith(diagnostic, episode, 2)
  })

  it('should show Notification when no more episodes to label', () => {
    const episode = { text: 'SOME_TEXT' }
    const nextEpisode = jest.fn()

    const component = mount(
      <Home
        user={{ name: 'SOME_NAME' }}
        getDiagnostics={jest.fn()}
        getEpisodes={jest.fn()}
        nextEpisode={nextEpisode}
        diagnostics={diagnostics}
        episode={episode}
      />
    )

    component.setProps({ episode: null })
  })

  it('should show Notification when no more episodes to label', () => {
    const episode = { text: 'SOME_TEXT' }
    const nextEpisode = jest.fn()

    const component = mount(
      <Home
        user={{ name: 'SOME_NAME' }}
        getDiagnostics={jest.fn()}
        getEpisodes={jest.fn()}
        nextEpisode={nextEpisode}
        diagnostics={diagnostics}
        episode={episode}
      />
    )

    component.setProps({ episode: null })
  })
})
