import React from 'react'
import AppConnected, { App } from './App'
import configureStore from 'redux-mock-store'
import { shallow } from 'enzyme'

const mockStore = configureStore([])

describe('App Tests', () => {
  it('should match snapshot', () => {
    const emptyStore = mockStore({ })
    const componentWithEmptyStore = shallow(<AppConnected store={emptyStore} />)
    expect(componentWithEmptyStore).toMatchSnapshot()

    const store = mockStore({ auth: { user: 'SOME_USER' } })
    const component = shallow(<AppConnected store={store} />)
    expect(component).toMatchSnapshot()
  })

  it('should render Login if no user exists', () => {
    const component = shallow(<App getDiagnostics={jest.fn()} />)
    expect(component).toMatchSnapshot()
  })

  it('should render Home if user exists', () => {
    const component = shallow(<App user={{ name: 'SOME_USER ' }} />)
    expect(component).toMatchSnapshot()
  })
})
