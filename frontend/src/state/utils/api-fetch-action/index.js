export const fetchRequest = (url, options) =>
  fetch(url, options)
    .then(async response => {
      const body = await response.json()

      if (response.ok) {
        if (body.status_code > 204) throw Error(body.message)
        return body
      }

      throw Error(body.message)
    })
    .then(json => {
      return json
    })
    .catch(error => {
      throw Error(error.message)
    })

export const fetchAction = ({ url, type, data, token }) => {
  const headers = {
    Accept: 'application/json',
    Authorization: `Bearer ${token}`,

    'Content-Type': 'application/json'
  }

  return fetchRequest(url, {
    body: JSON.stringify(data),
    method: type,
    headers
  })
}
