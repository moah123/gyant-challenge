import { applyMiddleware, compose, createStore } from 'redux'
import rootReducer from './reducers'
import thunk from 'redux-thunk'

let composeEnhancers = compose
/*eslint-disable */
if (
  typeof window !== 'undefined' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
}
/* eslint-enable */

export default createStore(
  rootReducer,
  undefined,
  composeEnhancers(applyMiddleware(thunk))
)
