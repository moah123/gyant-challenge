import {
  AUTHENTICATE_ERROR,
  AUTHENTICATE_REQUEST,
  AUTHENTICATE_SUCCESS,
  LOGOUT,
  GET_DIAGNOSIS_ERROR,
  GET_DIAGNOSIS_REQUEST,
  GET_DIAGNOSIS_SUCCESS,
  GET_EPISODES_ERROR,
  GET_EPISODES_REQUEST,
  GET_EPISODES_SUCCESS,
  NEXT_EPISODE,
  CREATE_USER_SUCCESS
} from '../constants'

const authData = localStorage.getItem('@gyant_auth')
const auth = authData ? JSON.parse(authData) : null

const initialState = {
  diagnosis: [],
  episodes: [],
  selectedEpisode: undefined,
  auth: auth
}

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATE_SUCCESS:
    case CREATE_USER_SUCCESS: {
      const auth = action.payload
      return {
        ...state,
        loading: false,
        auth
      }
    }
    case AUTHENTICATE_REQUEST:
    case GET_EPISODES_REQUEST:
    case GET_DIAGNOSIS_REQUEST: {
      return {
        ...state,
        loading: true
      }
    }
    case AUTHENTICATE_ERROR:
      return {
        ...state,
        loading: false,
        auth: null
      }
    case GET_DIAGNOSIS_ERROR: {
      return {
        ...state,
        loading: false,
        diagnosis: []
      }
    }
    case GET_DIAGNOSIS_SUCCESS: {
      const diagnosis = action.payload
      return {
        ...state,
        loading: false,
        diagnosis
      }
    }
    case GET_EPISODES_ERROR: {
      return {
        ...state,
        loading: false,
        episodes: []
      }
    }
    case GET_EPISODES_SUCCESS: {
      const episodes = action.payload
      return {
        ...state,
        loading: false,
        episodes,
        selectedEpisode: episodes && episodes.length > 0 ? episodes[0] : null
      }
    }
    case NEXT_EPISODE: {
      const episodes = state.episodes.slice(1)

      return {
        ...state,
        episodes,
        selectedEpisode: episodes.length > 0 ? episodes[0] : null
      }
    }
    case LOGOUT: {
      return {
        ...initialState,
        auth: null
      }
    }
    default:
      return state
  }
}
