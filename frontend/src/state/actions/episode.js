import {
  GET_EPISODES_REQUEST,
  GET_EPISODES_ERROR,
  GET_EPISODES_SUCCESS,
  LABEL_EPISODE_REQUEST,
  LABEL_EPISODE_SUCCESS,
  LABEL_EPISODE_ERROR,
  NEXT_EPISODE
} from '../constants'
import { fetchAction } from '../utils/api-fetch-action'

export const fetchEpisodes = () => (dispatch, getState) => {
  const { auth } = getState()
  const { token } = auth
  dispatch({ type: GET_EPISODES_REQUEST })
  return fetchAction({
    type: 'GET',
    token,
    url: `http://localhost:9000/v1/episode/all`
  })
    .then(data => dispatch({ type: GET_EPISODES_SUCCESS, payload: data }))
    .then(data => data.payload)
    .catch(err => {
      dispatch({ type: GET_EPISODES_ERROR, payload: err })
      throw err
    })
}

export const nextEpisode = (diagnostic, episode, time) => async (
  dispatch,
  getState
) => {
  const { auth } = getState()
  const { user, token } = auth

  dispatch({ type: LABEL_EPISODE_REQUEST })
  return fetchAction({
    data: {
      diagnosis: diagnostic._id,
      user: user._id,
      episode: episode._id,
      time
    },
    token,
    type: 'POST',
    url: `http://localhost:9000/v1/episode/label`
  })
    .then(data => {
      dispatch({ type: LABEL_EPISODE_SUCCESS, payload: data })
      dispatch({ type: NEXT_EPISODE })
    })
    .catch(err => {
      dispatch({ type: LABEL_EPISODE_ERROR, payload: err })
      throw err
    })
}
