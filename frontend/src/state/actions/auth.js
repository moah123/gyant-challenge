import {
  AUTHENTICATE_REQUEST,
  AUTHENTICATE_ERROR,
  AUTHENTICATE_SUCCESS,
  CREATE_USER_REQUEST,
  CREATE_USER_ERROR,
  CREATE_USER_SUCCESS,
  LOGOUT
} from '../constants'
import { fetchAction } from '../utils/api-fetch-action'

export const login = (email, password) => dispatch => {
  dispatch({ type: AUTHENTICATE_REQUEST })
  return fetchAction({
    data: {
      email,
      password
    },
    type: 'POST',
    url: `http://localhost:9000/v1/user/login`
  })
    .then(data => {
      dispatch({ type: AUTHENTICATE_SUCCESS, payload: data })
      localStorage.setItem('@gyant_auth', JSON.stringify(data))
    })
    .catch(err => {
      dispatch({ type: AUTHENTICATE_ERROR, payload: err })
      throw err
    })
}

export const logout = () => async (dispatch, getState) => {
  const { auth } = getState()
  const { token } = auth

  localStorage.setItem('@gyant_auth', null)
  dispatch({ type: LOGOUT })

  return fetchAction({
    type: 'POST',
    token,
    url: `http://localhost:9000/v1/user/logout`
  })
    .then(data => data)
    .catch(err => {
      dispatch({ type: AUTHENTICATE_ERROR, payload: err })
      throw err
    })
}

export const createUser = (email, password, name) => dispatch => {
  dispatch({ type: CREATE_USER_REQUEST })
  return fetchAction({
    data: {
      email,
      name,
      password
    },
    type: 'POST',
    url: `http://localhost:9000/v1/user`
  })
    .then(data => {
      dispatch({ type: CREATE_USER_SUCCESS, payload: data })
      localStorage.setItem('@gyant_auth', JSON.stringify(data))
    })
    .catch(err => {
      dispatch({ type: CREATE_USER_ERROR, payload: err })
      throw err
    })
}