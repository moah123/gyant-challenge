import {
  GET_DIAGNOSIS_REQUEST,
  GET_DIAGNOSIS_ERROR,
  GET_DIAGNOSIS_SUCCESS
} from '../constants'
import { fetchAction } from '../utils/api-fetch-action'

export const fetchDiagnostics = () => (dispatch, getState) => {
  const { auth } = getState()
  const { token } = auth

  dispatch({ type: GET_DIAGNOSIS_REQUEST })
  return fetchAction({
    type: 'GET',
    token,
    url: `http://localhost:9000/v1/diagnosis/all`
  })
    .then(data => dispatch({ type: GET_DIAGNOSIS_SUCCESS, payload: data }))
    .then(data => data.payload)
    .catch(err => {
      dispatch({ type: GET_DIAGNOSIS_ERROR, payload: err })
      throw err
    })
}
