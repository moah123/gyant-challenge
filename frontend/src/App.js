import React from 'react'
import './App.css'
import 'antd/dist/antd.css'
import { connect } from 'react-redux'
import Home from './containers/home'
import Login from './containers/login'

export function App({ user }) {
  return user ? <Home></Home> : <Login></Login>
}

const mapStateToProps = state => ({
  user: state.auth ? state.auth.user : null
})

export default connect(mapStateToProps)(App)
