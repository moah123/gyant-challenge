# Doctor Case Label

This is the result of Gyant EHR Labeling Challenge

**Sample User**

teste@gyant.com / 123456

### Technology

**Frontend**  React + Redux
**Backend** Node.JS + MongoDB
**Deploy** Docker

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

**Mandatory**

- [NodeJS 10+](https://nodejs.org/en/download/)
- [Docker](https://docs.docker.com/v17.09/engine/installation/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install/)

### Installing

Clone the repo

```
git clone git@bitbucket.org:moah123/gyant-challenge.git
```

```
yarn install
```

## Run for Development

```
docker-compose up 
```

Navigate to http://localhost:3000 and enjoy. :rocket:

### Running the tests

A test database is created for backend tests. In order to connect to that database, and perform required insert and read operations, the containers must be up.

Backend 

```
cd backend && yarn test
```

Frontend 

```
cd frontend && yarn test
```

**yarn test** generates a coverage report in ./coverage/lcov-report/index.html. This file contains code coverage information on Statements/Branches/Functions/Lines for every tested file. A 100% coverage is required in order to proceed and commit.

## Run for Production

```
docker-compose -f docker-compose-prod.yml build --no-cache --force-rm

docker-compose -f docker-compose-prod.yml up -d

```

## Run Seeders

In order to populate database with sample data for the solution to work: 

- Diagnosis
- Episodes
- Sample User 

you should execute seeders:

```
docker-compose exec gyant-api yarn md-seed run -d
```
