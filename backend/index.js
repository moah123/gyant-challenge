import Logger from './src/utils/logger';
import app from './src/app';
import config from 'config';
import shutdown from 'http-shutdown';
const { host, port } = config.get('server');

const server = shutdown(app.listen(port, host));
const log = new Logger();

server.on('listening', () => {
  log.info(`App started on http://${host}:${port}`);
});

if (process.send) {
  server.once('listening', () => {
    process.send('ready');
    log.info(`Application 'ready'`);
  });
}

log.info(`Application started.`);
