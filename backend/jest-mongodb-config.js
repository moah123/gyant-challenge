// eslint-disable-next-line no-undef
module.exports = {
  autoStart: false,
  binary: {
    skipMD5: true,
    version: '4.0.2',
  },
  mongodbMemoryServerOptions: {
    instance: {
      dbName: 'jest',
    },
  },
};
