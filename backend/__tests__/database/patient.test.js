import PatientModel from '../../database/models/patient';
import mongoose from 'mongoose';

const patientData = {
  name: 'Patient Name',
};

describe('Patient Model Test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      'mongodb://localhost:27017/gyant-test',
      { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
      async err => {
        const list = await mongoose.connection.db
          .listCollections({
            name: 'patient',
          })
          .toArray();

        if (list.length > 0) {
          mongoose.connection.db.dropCollection('patient');
        }

        if (err) {
          console.error(err);
          throw err;
        }
      }
    );
  });

  afterAll(async done => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.disconnect();
    done();
  });

  it('should create & save patient successfully', async () => {
    const validPatient = new PatientModel(patientData);
    const savedPatient = await validPatient.save();

    expect(savedPatient._id).toBeDefined();
    expect(savedPatient.name).toBe(patientData.name);
  });
});
