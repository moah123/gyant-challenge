import DiagnosisModel from '../../database/models/diagnosis';
import mongoose from 'mongoose';

const diagnosisData = {
  code: 'A094',
  description: 'Some Codition',
};

describe('Diagnosis Model Test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      'mongodb://localhost:27017/gyant-test',
      { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
      async err => {
        const list = await mongoose.connection.db
          .listCollections({
            name: 'diagnosis',
          })
          .toArray();

        if (list.length > 0) {
          mongoose.connection.db.dropCollection('diagnosis');
        }

        if (err) {
          console.error(err);
          throw err;
        }
      }
    );
  });

  afterAll(async done => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.disconnect();
    done();
  });

  it('should create & save diagnosis successfully', async () => {
    const validDiagnosis = new DiagnosisModel(diagnosisData);
    const savedDiagnosis = await validDiagnosis.save();

    expect(savedDiagnosis._id).toBeDefined();
    expect(savedDiagnosis.code).toBe(diagnosisData.code);
    expect(savedDiagnosis.description).toBe(diagnosisData.description);
  });
});
