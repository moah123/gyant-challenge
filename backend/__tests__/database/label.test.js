import LabelModel from '../../database/models/label';
import mongoose from 'mongoose';

const labelData = {
  date: new Date(),
  diagnosis: '5dba34406fbfe402b2c9fc1e',
  episode: '5dba34406fbfe402b2c9fc1e',
  time: 40,
  user: '5dba34406fbfe402b2c9fc1e',
};

describe('Label Model Test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      'mongodb://localhost:27017/gyant-test',
      { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
      async err => {
        const list = await mongoose.connection.db
          .listCollections({
            name: 'label',
          })
          .toArray();

        if (list.length > 0) {
          mongoose.connection.db.dropCollection('label');
        }

        if (err) {
          console.error(err);
          throw err;
        }
      }
    );
  });

  afterAll(async done => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.disconnect();
    done();
  });

  it('should create & save label successfully', async () => {
    const validLabel = new LabelModel(labelData);
    const savedLabel = await validLabel.save();

    expect(savedLabel._id).toBeDefined();
    expect(savedLabel.date).toBe(labelData.date);
    expect(savedLabel.time).toBe(labelData.time);
  });
});
