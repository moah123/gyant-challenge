import EpisodeModel from '../../database/models/episode';
import mongoose from 'mongoose';

const episodeData = {
  date: new Date(),
  patient_id: '5dba34406fbfe402b2c9fc1e',
  text: 'Episode Text',
};

describe('Episode Model Test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      'mongodb://localhost:27017/gyant-test',
      { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
      async err => {
        const list = await mongoose.connection.db
          .listCollections({
            name: 'episode',
          })
          .toArray();

        if (list.length > 0) {
          mongoose.connection.db.dropCollection('episode');
        }

        if (err) {
          console.error(err);
          throw err;
        }
      }
    );
  });

  afterAll(async done => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.disconnect();
    done();
  });

  it('should create & save episode successfully', async () => {
    const validEpisode = new EpisodeModel(episodeData);
    const savedEpisode = await validEpisode.save();

    expect(savedEpisode._id).toBeDefined();
    expect(savedEpisode.date).toBe(episodeData.date);
    expect(savedEpisode.text).toBe(episodeData.text);
  });
});
