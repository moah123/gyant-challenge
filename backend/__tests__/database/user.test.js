import UserModel from '../../database/models/user';
import mongoose from 'mongoose';

const userData = {
  email: 'email@example.com',
  name: 'Test User',
  password: '123456',
};

describe('User Model Test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      'mongodb://localhost:27017/gyant-test',
      { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
      async err => {
        const list = await mongoose.connection.db
          .listCollections({
            name: 'users',
          })
          .toArray();

        if (list.length > 0) {
          mongoose.connection.db.dropCollection('users');
        }

        if (err) {
          console.error(err);
          throw err;
        }
      }
    );
  });

  afterAll(async done => {
    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.disconnect();
    done();
  });

  it('should create & save user successfully', async () => {
    const validUser = new UserModel(userData);
    const savedUser = await validUser.save();

    expect(savedUser._id).toBeDefined();
    expect(savedUser.name).toBe(userData.name);
    expect(savedUser.email).toBe(userData.email);
  });

  it('should throw validation error if creating user without required field', async () => {
    const userWithoutRequiredField = new UserModel({ name: 'Test User' });

    let err;

    try {
      const savedUserWithoutRequiredField = await userWithoutRequiredField.save();

      err = savedUserWithoutRequiredField;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
  });

  it('should throw error if email is invalid', async () => {
    const userWithInvalidEmail = new UserModel({
      email: 'invalidemail',
      name: 'Test User',
      password: '123456',
    });

    let err;

    try {
      const savedUserWithInvalidEmail = await userWithInvalidEmail.save();

      err = savedUserWithInvalidEmail;
    } catch (error) {
      err = error;
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
  });

  it('should update user data', async () => {
    const user = await UserModel.findOne({ email: 'email@example.com' });

    user.name = 'New Name';

    const result = await user.save();

    expect(result.name).toBe('New Name');
  });
});
