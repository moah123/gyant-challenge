import { Seeder } from 'mongoose-data-seed';
import Diagnosis from '../models/diagnosis';
import data from './__diagnosis__.json';

class DiagnosisSeeder extends Seeder {
  async shouldRun() {
    const count = await Diagnosis.countDocuments().exec();

    return count === 0;
  }

  run() {
    return Diagnosis.create(data);
  }
}

export default DiagnosisSeeder;
