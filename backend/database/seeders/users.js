import { Seeder } from 'mongoose-data-seed';
import User from '../models/user';
import bcrypt from 'bcryptjs';
import data from './__users__.json';

class UsersSeeder extends Seeder {
  async shouldRun() {
    const count = await User.countDocuments().exec();

    return count === 0;
  }

  run() {
    return User.create(data);
  }
}

export default UsersSeeder;
