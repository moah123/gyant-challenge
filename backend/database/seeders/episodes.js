import { Seeder } from 'mongoose-data-seed';
import Episode from '../models/episode';
import data from './__episodes__.json';

class EpisodeSeeder extends Seeder {
  async shouldRun() {
    const count = await Episode.countDocuments().exec();

    return count === 0;
  }

  run() {
    return Episode.create(data);
  }
}

export default EpisodeSeeder;
