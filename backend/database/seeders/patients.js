import { Seeder } from 'mongoose-data-seed';
import Patient from '../models/patient';
import data from './__patients__.json';

class PatientsSeeder extends Seeder {
  async shouldRun() {
    const count = await Patient.countDocuments().exec();

    return count === 0;
  }

  run() {
    return Patient.create(data);
  }
}

export default PatientsSeeder;
