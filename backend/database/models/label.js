import mongoose from 'mongoose';
const { Schema } = mongoose;

const labelSchema = new Schema(
  {
    date: {
      defaut: new Date(),
      type: Date,
    },
    time: {
      type: Number,
      required: true,
    },
    diagnosis: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Diagnosis',
      required: true,
    },
    episode: {
      type: Schema.Types.ObjectId,
      ref: 'Episode',
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    }
  },
  {
    timestamps: true,
  }
);

const Label = mongoose.model('label', labelSchema);

export default Label;
