import mongoose from 'mongoose';
const { Schema } = mongoose;

const episodeSchema = new Schema(
  {
    date: {
      type: Date,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    patient_id: {
      type: Schema.Types.ObjectId,
      require: true,
    },
    labeledBy: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  },
  {
    timestamps: true,
  }
);

const Episode = mongoose.model('episode', episodeSchema);

export default Episode;
