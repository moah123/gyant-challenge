import mongoose from 'mongoose';
const { Schema } = mongoose;

const diagnosisSchema = new Schema({
  code: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    require: true,
  }
}, {
  timestamps: true
});

const Diagnosis = mongoose.model('diagnosis', diagnosisSchema);

export default Diagnosis;
