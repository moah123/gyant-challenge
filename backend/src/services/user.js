import User from '../../database/models/user';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

export default class UserService {
  async createUser(data) {
    const user = new User(data);

    await user.save();

    return user;
  }

  async findByCredentials(email, password) {
    const user = await User.findOne({ email });

    if (!user) {
      throw new Error('Invalid login credentials');
    }

    const isPasswordMatch = await bcrypt.compare(password, user.password);

    if (!isPasswordMatch) {
      throw new Error('Invalid login credentials');
    }

    return user;
  }

  async generateToken(user) {
    const JWT_KEY = process.env.JWT_KEY || 'GYANT2020';
    const token = jwt.sign(
      { _id: user._id },
      JWT_KEY
    );
    const { tokens } = user;

    user.tokens = tokens.concat({ token });
    await user.save();

    return token;
  }
}
