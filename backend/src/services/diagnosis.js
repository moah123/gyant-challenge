import Diagnosis from '../../database/models/diagnosis';

export default class DiagnosisService {
  async findAll() {
    const diagnostics = await Diagnosis.find();

    return diagnostics;
  }
}
