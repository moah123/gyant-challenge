import Label from '../../database/models/label';

export default class LabelService {
  async createLabel(data) {
    const label = new Label(data);

    await label.save();

    return label;
  }
}
