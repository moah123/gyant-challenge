import Episode from '../../database/models/episode';

export default class EpisodeService {
  async findAll() {
    const episodes = await Episode.find();

    return episodes;
  }

  async findById(id) {
    return await Episode.findById(id);
  }
}
