import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import routes from './routes';

const app = express();

app.use(cors());
app.use(express.json());
app.use(bodyParser.json());

const versions = routes(app, express);

versions.forEach(ve => {
  const { version, routes } = ve;

  routes.forEach(ro => {
    const { name, route } = ro;

    app.use(`/${version}/${name}`, route);
  });
});

var connectWithRetry = function() {
  return mongoose.connect(
    process.env.MONGO_URL || 'mongodb://gyant-db:27017/gyant',
    {
      useNewUrlParser: true,
    },
    function(err) {
      if (err) {
        console.error(
          'Failed to connect to mongo on startup - retrying in 1 sec',
          err
        );
        setTimeout(connectWithRetry, 1000);
      } else {
        console.log('-- DB Connected -- ');
      }
    }
  );
};
connectWithRetry();

export default app;
