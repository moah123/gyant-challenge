import jwt from 'jsonwebtoken';
import users from '../../database/models/user';

const auth = async (req, res, next) => {
  const header = req.header('Authorization');

  try {
    if (!header) {
      throw new Error();
    }
    const token = header.replace('Bearer ', '');
    const JWT_KEY = process.env.JWT_KEY || 'GYANT2020';
    // eslint-disable-next-line no-process-env
    const data = jwt.verify(token, JWT_KEY);

    const user = await users.findOne({ _id: data._id, 'tokens.token': token });

    if (!user) {
      throw new Error();
    }

    // eslint-disable-next-line require-atomic-updates
    req.user = user;
    // eslint-disable-next-line require-atomic-updates
    req.token = token;

    next();
  } catch (error) {
    res.status(401).send({ error: 'Not authorized to access this resource' });
  }
};

export default auth;
