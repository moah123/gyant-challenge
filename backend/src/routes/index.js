import v1 from './v1';

export default (app, express) => {
  return [
    {
      routes: v1(app, express),
      version: 'v1'
    }
  ];
};
