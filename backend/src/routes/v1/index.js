import Diagnosis from './diagnosis';
import Episode from './episode';
import User from './user';

export default (app, express) => [
  { name: 'diagnosis', route: new Diagnosis(app, express) },
  { name: 'episode', route: new Episode(app, express) },
  { name: 'user', route: new User(app, express) },
];
