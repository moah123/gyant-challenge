import DiagnosisService from '../../services/diagnosis';
import Logger from '../../utils/logger';
import auth from '../../middleware/auth';

export default class Diagnosis {
  constructor(app, express) {
    this.logger = new Logger();
    this.diagnosisService = new DiagnosisService();

    // eslint-disable-next-line new-cap
    const api = express.Router();

    api.get('/all', auth, async (req, res) => {
      try {
        const diagnostics = await this.diagnosisService.findAll();

        res.status(200).send(diagnostics);
      } catch (error) {
        res.status(400).send({ message: error.message });
      }
    });

    return api;
  }
}
