import EpisodeService from '../../services/episode';
import LabelService from '../../services/label';
import Logger from '../../utils/logger';
import auth from '../../middleware/auth';

export default class Episode {
  constructor(app, express) {
    this.logger = new Logger();
    this.episodeService = new EpisodeService();
    this.labelService = new LabelService();

    // eslint-disable-next-line new-cap
    const api = express.Router();

    api.get('/all', auth, async (req, res) => {
      try {
        const episodes = await this.episodeService.findAll();

        const result = episodes.filter(episode => episode.labeledBy.indexOf(req.user._id) === -1)

        res.status(200).send(result);
      } catch (error) {
        res.status(400).send({ message: error.message });
      }
    });

    api.post('/label', auth, async (req, res) => {
      try {
        const label = await this.labelService.createLabel(req.body);

        const episode = await this.episodeService.findById(req.body.episode);

        const found = episode.labeledBy.find(item => item === req.body.user);

        if (!found) {
          episode.labeledBy.push(req.body.user);
          await episode.save();
        }

        res.status(201).send(label);
      } catch (error) {
        res.status(400).send({ message: error.message });
      }
    });

    return api;
  }
}
