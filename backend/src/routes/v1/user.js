import Logger from '../../utils/logger';
import UserService from '../../services/user';
import auth from '../../middleware/auth';

export default class User {
  constructor(app, express) {
    this.logger = new Logger();
    this.userService = new UserService();

    // eslint-disable-next-line new-cap
    const api = express.Router();

    api.post('/', async (req, res) => {
      try {
        const user = await this.userService.createUser(req.body);

        const token = await this.userService.generateToken(user);

        res.status(201).send({ token, user });
      } catch (error) {
        res.status(400).send({ message: error.message });
      }
    });

    api.post('/login', async (req, res) => {
      const { email, password } = req.body;

      try {
        const user = await this.userService.findByCredentials(email, password);

        const token = await this.userService.generateToken(user);

        this.logger.info(`User Login`);

        res.status(200).send({
          token,
          user,
        });
      } catch (error) {
        res.status(400).send({ message: error.message });
      }
    });

    api.post('/logout', auth, async (req, res) => {
      try {
        req.user.tokens = req.user.tokens.filter(token => {
          return token.token !== req.token;
        });
        await req.user.save();
        res.status(200).send(true);
      } catch (error) {
        res.status(400).send({ message: error.message });
      }
    });

    return api;
  }
}
