import bunyan from 'bunyan';

export default class Logger {
  constructor(context) {
    return bunyan.createLogger({
      name: context ? `gyant-api:${context}` : 'gyant-api',
      streams: [
        {
          level: 'trace',
          stream: process.stdout,
        },
        {
          level: 'debug',
          stream: process.stdout,
        },
        {
          level: 'info',
          stream: process.stdout,
        },
        {
          level: 'warn',
          stream: process.stderr,
        },
        {
          level: 'error',
          stream: process.stderr,
        },
        {
          level: 'fatal',
          stream: process.stderr,
        },
      ],
    });
  }
}
