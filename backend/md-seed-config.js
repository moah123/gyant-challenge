import mongoose from 'mongoose';
import Diagnosis from './database/seeders/diagnosis';
import Episodes from './database/seeders/episodes';
import Patients from './database/seeders/patients';
import Users from './database/seeders/users';

const mongoURL = process.env.MONGO_URL || 'mongodb://gyant-db:27017/gyant';

export const seedersList = {
  Patients,
  Episodes,
  Diagnosis,
  Users,
};

export const connect = async () =>
  await mongoose.connect(mongoURL, { useNewUrlParser: true });

export const dropdb = async () => mongoose.connection.db.dropDatabase();
