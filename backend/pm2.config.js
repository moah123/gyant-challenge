// eslint-disable-next-line no-undef
module.exports = {
  apps: [
    {
      args: '',
      autorestart: true,
      cwd: '/app',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
      env_staging: {
        NODE_ENV: 'staging',
      },
      instances: 1,
      max_memory_restart: '1G',
      name: 'gyant-api',
      script: 'node -r esm index.js',
      version: '0.1.0',
      watch: false,
    }
  ],
};
